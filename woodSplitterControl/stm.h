#ifndef STM_H
#define STM_H
#include "stmClmp.h"
#include "stmSaw.h"
#include "stmCon.h"
#include "stmAuto.h"


typedef enum {
  mod_0_MANUAL,
  mod_1_AUTO,
  mod_2_PAUSE
} modes;

modes mode = mod_0_MANUAL;

bool modeAuto;
bool modeManual;
bool modePauseAuto;

//state bools

bool openClamp_stm;
bool liftSaw_stm;
bool runSaw_stm;
bool closeClamp_stm;

bool logLifterUp_stm;
bool logLifterDown_stm;
bool crossConveyorForward_stm;
bool crossConveyorBackward_stm;

void calcMode();
void calcStates();
#endif
