#ifndef STM_CON_H
#define STM_CON_H


typedef enum {
  con_0_CONUNDEF,
  con_1_LIFT_CONVEYOR,
  con_2_CONVEYOR_FRONT_WAIT,
  con_3_CONVEYOR_FRONT,
  con_4_REVERT_CONVEYOR,
  con_5_CONVEYOR_BACK_WAIT,
  con_6_CONVEYOR_BACK
} conveyorStates;

conveyorStates conveyorState = con_0_CONUNDEF;

void calcConveyorState();
#endif //STM_CON_H
