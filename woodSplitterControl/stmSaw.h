#ifndef STM_SAW_H
#define STM_SAW_H

#include "Timebase.h"
#include "hFun.h"


typedef enum {
  saw_0_SAWUNDEF,
  saw_1_LIFT,
  saw_2_UP,
  saw_3_RUN,
  saw_4_DOWN,
  saw_5_PAUSE_LIFT,
  saw_6_PAUSE_RUN
} sawStates;

sawStates sawState = saw_0_SAWUNDEF;

bool sawSelected;
bool sawDone;

void calcSawStates();
#endif
