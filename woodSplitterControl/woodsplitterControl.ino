/*
   DEBUG = 0 ... OFF
   DEBUG = 1 ... MINIMAL
   DEBUG = 2 ... EXTENDED
   DEBUG = 3 ... ALL INPUTS/OUTPUTS
*/
#define DEBUG 1
#define SIM
#define LOG
#define PDA

#include <Controllino.h>
#include "globals.h"
#include "IO_handling.h"
#include "hFun.h"
#include "settings.h"
#include "stm.h"

//variables for debug messages
int debugCount = 0;
bool debugC1; //triggered every cycle
bool debugC2; //triggered every 2nd cycle
bool debugC5; //triggered every 5th cycle
bool debugC10; //triggered every 10th cycle


float speedFdb ; //
float accFdb;

/*
    setup code at arduino startup
*/
void setup() {
  InitTimebase();
  //initialize circular buffer
  sCirBuf.t_diff[0] = 0;
  sCirBuf.omega[0] = 0.0f;
  sCirBuf.curPos = 0;

  TON_init(0, 1, 1.5); //PB_startAuto skip crossconveyor
  TON_init(1, 1, 1); //PB_reset time
  TON_init(2, 1, 15); //auto saw time
  TON_init(3, 0, 250); //crossconveyor wait time (recyled for forward and backward ;-)
  TON_init(4, 1, 1); //wait for wood
  TON_init(5, 1, 1); //delay PS_saw
  TON_init(28, 0, 30); //interupt test
  TON_init(29, 1, 1); //UDP send intervall timer 1s
  
  sRamp.min = 30.0;
  sRamp.max = 200.0;
  sRamp.slopeUp = 0.05; // output change per ms
  sRamp.slopeDown = 0.05;

  PT1_init(0, 2000.0, 2000.0); // T_rise [ms], T_fall [ms] is somewhere used for VFD setpoint
  PT1_init(1, 500.0, 500.0); // T_rise [ms], T_fall [ms] test
  PT1_init(2, 500.0, 500.0); // T_rise [ms], T_fall [ms] test
  PT1_init(3, 15.0, 15.0); // T_rise [ms], T_fall [ms] test

  speedSetpoint = SPD_IDLE;

#if defined (SIM) || defined(PDA) || defined(LOG) || DEBUG>0
  // start the Ethernet and UDP:
  Ethernet.begin(mac, locIp);
  Udp.begin(locPort);
#endif

#if  DEBUG>0
  Serial.begin(9600);
#endif

  setupIO();
}

bool sawPosOK() {
  int sawPosMin = SAW_POSITION - SAW_POS_DEV;
  int sawPosMax = SAW_POSITION + SAW_POS_DEV;
  return (driveDistance >= sawPosMin && driveDistance <= sawPosMax);
}

void loop() {

  if (++debugCount > 10) {
    debugCount = 1;
  }

  debugC1 = !(debugCount % 1);
  debugC2 = !(debugCount % 2);
  debugC5 = !(debugCount % 5);
  debugC10 = !(debugCount % 10);



  //TODO adapt with care
  //prevent
  // delay(10);

  //  Serial.println(sSystem.cyclTime);

  UpdateTimebase();
  readInput();
  calcStates(); //call sequences

#if DEBUG>0
  //  if ( debugC10 ) {
  //    writeDLog(String((-3 + 16)%16)) ;
  //  }
#endif


  omegaEval(&speedFdb, &accFdb);


  //max speed depending on drive distance or systeme pressure which ever gives smaller number
  //factor is max possible distance in % depending on distance in mmm

  //velocity to keep even near to endposition

  float minSplitSpeed = max((ENDPOS_FWD - driveDistance) * 1.1, 30.0);

  sRamp.max = min(maxSpeed(systemPressure), O_driveForward ? minSplitSpeed : MAXFLOAT);

  speedRamp = ramp(speedSetpoint);
  rel_O_speed = PT1(0, speedRamp);

  if (PB_reset) { //reset command bits from state-machine when sequence is aborted

    mode = mod_0_MANUAL;
    sawState = saw_0_SAWUNDEF;
    clampState = cls_0_CLMPUNDEF;
    conveyorState = con_0_CONUNDEF;
    action = act_0_NONE;
    state = sts_0_AUTO_OFF;
    sawSelected = LOW;

    AUTO_closeClamp = LOW;
    AUTO_openClamp = LOW ;
    AUTO_liftSaw = LOW;
    AUTO_saw = LOW;
    AUTO_driveForward = LOW;
    AUTO_driveBackward = LOW;
    stateChangeTriggered = false;
    AUTO_liftConveyor = LOW;
    AUTO_revertConveyor = LOW;
    TON(2, 0); //reset max saw time
#ifdef LOG
    writeLog("RESET: mode = MANUAL");
    writeLog("RESET: sawState = SAWUNDEF");
    writeLog("RESET: clampState = CLMPUNDEF");
    writeLog("RESET: conveyorState = CONUNDEF");
#endif
  }


  //pause sets ilk, all other buttons resets it
  if (PB_driveForward || PB_driveBackward || PB_closeClamp || PB_saw || PB_logLifterUp || PB_logLifterDown || PB_crossConveyor || PB_startAuto_RT) {
    gen_ilk = LOW;
  }
  if (PB_pauseAuto) {
    gen_ilk = HIGH;
  }
  runSaw_ilk = !sawPosOK() || clampState != cls_6_CLOSED;
  driveForward_ilk = (sawState != saw_2_UP);
  driveBackward_ilk = (sawState != saw_2_UP) || (driveDistance <= ENDPOS_BWD);
  crossConveyorForward_ilk = driveDistance >= (ENDPOS_BWD + ENDPOS_DEV);

  O_driveForward = ((PB_driveForward && !PB_driveBackward && (modeManual || modePauseAuto)) || AUTO_driveForward) && !driveForward_ilk && !gen_ilk;
  O_driveBackward = ((!PB_driveForward && PB_driveBackward && (modeManual  || modePauseAuto)) || AUTO_driveBackward) && !driveBackward_ilk && !gen_ilk;

  //speed setpoint for manual drive movement
  driveManualMove = (O_driveForward || O_driveBackward)  && modeManual;
  if (driveManualMove) {
    speedSetpoint = SPD_DRV_FWD;
  }
  if (F_TRIG(2, driveManualMove)) {
    speedSetpoint = SPD_IDLE;
  }

  O_closeClampSaw = (closeClamp_stm || (runSaw_stm && !runSaw_ilk)) && !gen_ilk;
  O_saw = (liftSaw_stm || runSaw_stm) && !gen_ilk;
  O_openClampLiftSaw = (openClamp_stm || liftSaw_stm ) && !gen_ilk;
  O_logLifterUp = ((PB_logLifterUp && (modeManual  || modePauseAuto)) || logLifterUp_stm ) && !gen_ilk;

  O_logLifterDown = ((PB_logLifterDown && (modeManual  || modePauseAuto)) || logLifterDown_stm ) && !gen_ilk ;
  if (O_logLifterUp || O_logLifterDown) {
    speedSetpoint = SPD_LOG_LIFTER;
  }
  if (F_TRIG(3, driveManualMove)) {
    speedSetpoint = SPD_IDLE;
  }
  O_crossConveyorForward = crossConveyorForward_stm && !crossConveyorForward_ilk && !gen_ilk;
  O_crossConveyorBackward = crossConveyorBackward_stm && !gen_ilk;

  writeOutput();
#ifdef PDA
  PDA_write();
#endif
}
