#include "Timebase.h"
#include "hFun.h"



void calcMode() {
  bool calcState;
  for (int i = 0; i < 2; i++) {
    calcState = (i == 0);
    switch (mode) {
      case mod_0_MANUAL:
        if (calcState) {
          if (PB_startAuto_RT && (conveyorState == con_3_CONVEYOR_FRONT || conveyorState == con_6_CONVEYOR_BACK || conveyorState == con_0_CONUNDEF)) {
            mode = mod_1_AUTO;
            state = sts_6_INIT_AUTO;
            action = act_8_LIFT_SAW; //initial initialisation state
            modeManual = false;
#ifdef LOG
            writeLog("mode = mod_1_AUTO");
            writeLog("state = sts_6_INIT_AUTO");
            writeLog("action = act_8_LIFT_SAW");
#endif
          }
        } else {
          modeManual = true;
          modeAuto = false;
          modePauseAuto = false;
        }
        break;
      case mod_1_AUTO:
        if (calcState) {
          if (PB_pauseAuto || PB_crossConveyor || PB_logLifterUp || PB_logLifterDown) {
            mode = mod_2_PAUSE;
#ifdef LOG
            writeLog("mode = PAUSE");
#endif
          }
        } else {
          modeManual = false;
          modeAuto = true;
          modePauseAuto = false;
        }
        break;
      case mod_2_PAUSE:
        if (calcState) {
          if (PB_startAuto_RT && (conveyorState == con_3_CONVEYOR_FRONT || conveyorState == con_6_CONVEYOR_BACK || conveyorState == con_0_CONUNDEF)) {
            mode = mod_1_AUTO;
            TON(2, 0); //reset max saw time
#ifdef LOG
            writeLog("mode = AUTO");
#endif
          }
        } else {
          modeManual = false;
          modeAuto = false;
          modePauseAuto = true;
        }
        break;
    }
  }
}

void calcStates() {
  sequenceAuto();
  calcClampStates();
  calcSawStates();
  calcConveyorState();
  calcMode();
}
