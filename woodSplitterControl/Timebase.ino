#include "Timebase.h"

void InitTimebase()
{
  sSystem.milli = millis();
  sSystem.micro = micros();
  sSystem.cyclTime = 0;
  sSystem.cyclTicks = 0;
  sSystem.milli_prev = sSystem.milli;
  sSystem.micro_prev = sSystem.micro;
}

void UpdateTimebase() //is used by TON, ...
{
  sSystem.milli_prev = sSystem.milli;
  sSystem.milli = millis();
  sSystem.micro = micros();
  sSystem.cyclTime = sSystem.milli - sSystem.milli_prev;
  if (sSystem.micro >= sSystem.micro_prev) {
    sSystem.cyclTicks = sSystem.micro - sSystem.micro_prev;
  } else {
    sSystem.cyclTicks = 0xffffffff - sSystem.micro_prev + sSystem.micro;
  }
}
