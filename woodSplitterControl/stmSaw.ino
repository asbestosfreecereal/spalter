#include "stmSaw.h"

/*
   calculates the current sawState
*/
void calcSawStates() {
  bool calcState;
  for (int i = 0; i < 2; i++) {
    calcState = (i == 0);
    switch (sawState) {
      case saw_0_SAWUNDEF:
        if (calcState) {
          if ((PB_saw && modeManual && !PB_closeClamp)
              || AUTO_liftSaw) {
            sawState = saw_1_LIFT;
#ifdef LOG
            writeLog("sawState = LIFT");
#endif
          }
        }
        else {
          liftSaw_stm = LOW;
          runSaw_stm = LOW;
        }
        break;
      case saw_1_LIFT:
        if (calcState) {
          if (modePauseAuto) {
            sawState = saw_5_PAUSE_LIFT;
          } else if (systemPressure > MAX_PRESSURE_SAW) {
            sawState = saw_2_UP;
            speedSetpoint = SPD_IDLE;
#ifdef LOG
            writeLog("sawState = UP");
#endif
          } else if (modeManual && PB_saw && clampClosed) {
            sawState = saw_3_RUN;
#ifdef LOG
            writeLog("sawState = RUN");
#endif
          }
        } else {
          liftSaw_stm = HIGH;
          runSaw_stm = LOW;
          sawSelected = HIGH;
          speedSetpoint = SPD_LIFT_SAW;
        }
        break;
      case saw_2_UP:
        if (calcState) {
          if ((modeManual && PB_saw && clampClosed)
              || (modeAuto && AUTO_saw && clampClosed)) {
            sawState = saw_3_RUN;
#ifdef LOG
            writeLog("sawState = RUN");
#endif
          }
        } else {
          liftSaw_stm = LOW;
          runSaw_stm = LOW;
          sawSelected = LOW;
        }
        break;
      case saw_3_RUN:
        if (calcState) {
          if (modePauseAuto) {
            sawState = saw_6_PAUSE_RUN;
          } else if (TON(5, PS_sawEndPos)) {
            TON(5, 0);
            sawState = saw_4_DOWN;
            speedSetpoint = SPD_IDLE;
#ifdef LOG
            writeLog("sawState = DOWN");
#endif
          } else if (modeManual && !PB_saw) {
            sawState = saw_1_LIFT;
#ifdef LOG
            writeLog("sawState = LIFT");
#endif
          }
        } else {
          liftSaw_stm = LOW;
          runSaw_stm = HIGH;
          sawSelected = HIGH;
          speedSetpoint = SPD_RUN_SAW;
        }
        break;
      case saw_4_DOWN:
        if (calcState) {
          if ((modeManual && (!PB_saw || !PB_closeClamp) )
              || (modeAuto && AUTO_liftSaw)) {
            sawState = saw_1_LIFT;
#ifdef LOG
            writeLog("sawState = LIFT");
#endif
          }
        } else {
          liftSaw_stm = LOW;
          runSaw_stm = LOW;
          sawSelected = HIGH;
        }
        break;
      case saw_5_PAUSE_LIFT:
        if (calcState) {
          if (!modePauseAuto) {
            sawState = saw_1_LIFT;
          }
        } else {
          liftSaw_stm = LOW;
          runSaw_stm = LOW;
          sawSelected = LOW;
        }
        break;
      case saw_6_PAUSE_RUN:
        if (calcState) {
          if (!modePauseAuto) {
            sawState = saw_3_RUN;
          }
        } else {
          liftSaw_stm = LOW;
          runSaw_stm = LOW;
          sawSelected = LOW;
        }
        break;
    }
  }
}
