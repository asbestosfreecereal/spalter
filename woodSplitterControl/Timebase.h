#ifndef TIMEBASE_H
#define TIMEBASE_H

struct {
  unsigned long milli;
  unsigned long milli_prev;
  unsigned long micro;
  unsigned long micro_prev;
  int cyclTime;  //cycle time
  unsigned int cyclTicks;  //cycle time
} sSystem;


void UpdateTimebase();

#endif
