#ifndef HFUN_H
#define HFUN_H

//#include "globals.h"

#define MAXTON 30
#define MAXRTRIG 30
#define MAXFTRIG 30
#define MAXPT1 4
#define MAXDIFF 2
#define CIRCBUF 128

#include "Timebase.h"

/* Overview used TON
   TON 1 ... PB_reset
   TON 2 ... auto saw time
*/

/* Overview used R_TRIG
   R_TRIG 1 ... PB_startAuto_RT
   R_TRIG 2 ...
*/

/* Overview used R_TRIG
   F_TRIG 1 ... PB_reset
   F_TRIG 2 ... driveManualMove
*/

static struct {
  //  unsigned long t_micros[CIRCBUF];
  unsigned int t_diff[CIRCBUF];
  float omega[CIRCBUF];
  float alpha[CIRCBUF];
  byte curPos;
} sCirBuf;

static struct {
  unsigned long startTime;
  unsigned long period;
  byte parameter;
  byte status;    //1=started, 2=ellapsed
} sTON[MAXTON];

static struct {
  float prevValue;
} sDIFF[MAXDIFF];


static struct {
  float curValue;
  float T_rise;
  float T_fall;
} sPT1[MAXPT1];

static struct {
  byte oldState;
} sR_TRIG[MAXRTRIG];

static struct {
  byte oldState;
} sF_TRIG[MAXFTRIG];
struct {
  float slopeUp; //percentage per ms
  float slopeDown; //percentage per ms
  float out;
  float min;
  float max;
} sRamp;

float maxSP[6][2] = {
  {33, 200},
  {60, 180.8},
  {109, 157.36},
  {148.6, 141.4},
  {174, 121.48},
  {205, 97.26 }
};

float tSpeedSetpoint; //test speed setpoint from simulation pannel

//semaphore
bool loopIsReading;
bool doNotUseData;


bool R_TRIG(byte Nr, bool State);
bool F_TRIG(byte Nr, bool State);
void TON_init(byte Nr, byte timeBase, float timeVal);
void TON_setTime(byte Nr, byte timeBase, float timeVal);
bool TON(byte Nr, bool Start);
float DIFF(byte Nr, float value);
float PT1(byte Nr, float Input);
void PT1_init(byte Nr, float T_rise, float T_fall);
float rampSet(float in);
float ramp(float in);
float maxSpeed(float sysPress);
void writeLog(const char * msg);
void writeDLog(String msg);
void freqInterupt();
bool freqEval(float * Freq);
void omegaEval(float * speedFdb, float * accFdb);
#ifdef PDA
void PDA_write();
#endif //PDA
#endif //HFUN_H
