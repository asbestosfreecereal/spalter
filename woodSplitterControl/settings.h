#ifndef SETTINGS_H
#define SETTINGS_H


//TODO determine max pressure possibly turn into floats if there are floating point values
const int SAW_POSITION = 230; //saw position on the wooden inlay [mm]
const int SAW_POS_DEV = 20;  //max deviation to not miss the wooden inlay [mm]
const int ENDPOS_BWD = 200; //travel back end position [mm]
const int ENDPOS_FWD = 750; //travel forward end position [mm]
const int ENDPOS_DEV = 10; //max travel deviation for end positions [mm]
const int FWD_LENGTH_CLMP = 100; //length to drive forward to provide grip for clamp [mm]
const int MIN_FWD_LENGTH_CLMP = 20; // minimum length to drive forward to provide grip for clamp only enough for front half of clamp to grip

const int MAX_PRESSURE_CLAMP = 100;  
const int MAX_PRESSURE_CONVEYOR = 100;
const int MAX_PRESSURE_FORWARD = 100;
const int MIN_DISTANCE = 200;
const int MAX_DISTANCE = 400;
const int MAX_PRESSURE_SAW = 100;
const int MAX_PRESSURE_LOG_LIFTER = 100;
const int FIFTY_CM = 500;
const int TWENTYFIVE_CM = 250;
const int THIRTYTHREE_CM =  330;

const float MAXFLOAT=3.4028235E+38;
const int FIVE_CM = 50;

//speed as percentage of rated motor speed
const float SPD_DRV_BWD_FAST = 200.0;
const float SPD_DRV_BWD = 100.0;  //not used now
const float SPD_DRV_FWD_FAST = 200.0;
const float SPD_DRV_FWD = 100.0;
const float SPD_CONV = 40.0;
const float SPD_IDLE = 40.0;
const float SPD_CLMP = 100;
const float SPD_RUN_SAW = 200.0;
const float SPD_LIFT_SAW = 100.0;
const float SPD_LOG_LIFTER = 40.0;

//hydraulic aggregate data

#define MOT_N 750 //rated rpm
#define MOT_P 7.5 //rated power kW
#define VDF_M 0.5 //
#define MEC_J 0.15 //kgm²
#define PUMP 32 // cm³/r


#endif // SETTINGS_H
