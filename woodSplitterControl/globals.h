#ifndef GLOBAL_H
#define GLOBAL_H


#if defined (SIM) || defined(PDA) || defined(LOG) || DEBUG>0

#include <Ethernet.h>
#include <EthernetUdp.h>

// IP, MAC and listening port
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFA, 0xED};

IPAddress locIp(192, 168, 10, 177);
IPAddress remIp(192, 168, 10, 150);
//IPAddress remIp(192, 168, 10, 169);
IPAddress PDA_Ip(192, 168, 10, 171);
unsigned const int locPort = 8888;
unsigned const int remPort = 8887;
unsigned const int PDA_Port = 5010;

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,

EthernetUDP Udp;
#endif


//variables for test purpose
//float testFloat;
//float testDouble;


//interlocks
bool gen_ilk;
bool runSaw_ilk;
bool driveForward_ilk;
bool driveBackward_ilk;
bool crossConveyorForward_ilk;

bool driveManualMove;

#endif // GLOBAL_H
