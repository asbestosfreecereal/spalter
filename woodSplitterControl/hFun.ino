#include "Timebase.h"
#include "hFun.h"

void TON_init(byte Nr, byte timeBase, float timeVal)
{
  sTON[Nr].parameter = timeBase;
  switch (timeBase) {
    case 0:     //ms
      sTON[Nr].period = timeVal;
      break;
    case 1:     //s
      sTON[Nr].period = 1000 * timeVal;
      break;
    case 2:     //min
      sTON[Nr].period = 60000 * timeVal;
      break;
    case 3:     //h
      sTON[Nr].period = 3600000 * timeVal;
      break;
    default:
      ;
  }
  sTON[Nr].status = 0;
  return;
}

void TON_setTime(byte Nr, byte timeBase, float timeVal)
{
  sTON[Nr].parameter = timeBase;
  switch (timeBase) {
    case 0:     //ms
      sTON[Nr].period = timeVal;
      break;
    case 1:     //s
      sTON[Nr].period = 1000 * timeVal;
      break;
    case 2:     //min
      sTON[Nr].period = 60000 * timeVal;
      break;
    case 3:     //h
      sTON[Nr].period = 3600000 * timeVal;
      break;
    default:
      ;
  }
  return;
}


bool TON(byte Nr, bool Start)
{
  if (!Start) {
    sTON[Nr].status = 0;
    return 0;
  } else if (sTON[Nr].status == 1) {  //Start and started -> check if elapsed
    unsigned long diff = sSystem.milli - sTON[Nr].startTime;
    if (diff >= sTON[Nr].period) {  //time elapsed
      sTON[Nr].status = 2;
      return 1;
    } else
      return 0;
  } else if (sTON[Nr].status == 2) {  //Start and elapsed
    return 1;
  } else if (sTON[Nr].status == 0) {  //Start timer
    sTON[Nr].startTime = sSystem.milli;
    sTON[Nr].status = 1;
    return 0;
  } else
    return 0;
}

float DIFF(byte Nr, float value) {
  float calcValue = (value - sDIFF[Nr].prevValue) / (float)sSystem.cyclTime * 1E3;
  sDIFF[Nr].prevValue = value;
  return calcValue;
}

void PT1_init(byte Nr, float T_rise, float T_fall) {
  sPT1[Nr].T_rise = T_rise;
  sPT1[Nr].T_fall = T_fall;
}

float PT1(byte Nr, float Input) {
  float curValue = sPT1[Nr].curValue;
  float curCycT = sSystem.cyclTime;
  float T_rf = Input > curValue ? sPT1[Nr].T_rise : sPT1[Nr].T_fall; //time rise or fall
  curValue += (curCycT / T_rf) * (Input - curValue);
  sPT1[Nr].curValue = curValue;
  return curValue;
}


bool R_TRIG(byte Nr, bool State) {
  byte oldState = sR_TRIG[Nr].oldState;
  sR_TRIG[Nr].oldState = State;
  return State && ! oldState;
}


bool F_TRIG(byte Nr, bool State) {
  byte oldState = sF_TRIG[Nr].oldState;
  sF_TRIG[Nr].oldState = State;
  return !State && oldState;
}

float rampSet(float in) {
  sRamp.out = in;
  return sRamp.out;
}

float ramp(float in) {
  float eps = 0.01; //TODO evaluate min epsilon for 200.0
  if (in < sRamp.out) {
    sRamp.out -= sSystem.cyclTime * sRamp.slopeUp;
    if (sRamp.out < in + eps) sRamp.out = in;
  } else if (in > sRamp.out) {
    sRamp.out += sSystem.cyclTime * sRamp.slopeDown;
    if (sRamp.out > in - eps) sRamp.out = in;
  }
  if (sRamp.out > sRamp.max) {
    sRamp.out = sRamp.max;
  } else if (sRamp.out < sRamp.min) {
    sRamp.out = sRamp.min;
  }
  return sRamp.out;
}

float maxSpeed(float sysPress) {

  int row = 0;
  while (sysPress > maxSP[row + 1][0]) {
    row++;
  }
  return ((sysPress - maxSP[row][0]) * maxSP[row + 1][1] + (maxSP[row + 1][0] - sysPress) * maxSP[row][1]) / (maxSP[row + 1][0] - maxSP[row][0]);
}

#ifdef LOG
void writeLog(const char * msg) {
  char sBuffer[1024];
  sBuffer[0] = 35;
  strcpy (&sBuffer[1], msg);
  Udp.beginPacket(remIp, remPort);
  Udp.write(sBuffer);
  Udp.endPacket();
}
#endif

#if DEBUG>0
void writeDLog(String msg) {
  char sBuffer[1024];
  sBuffer[0] = 120;
  strcpy (&sBuffer[1], msg.c_str());
  Udp.beginPacket(remIp, remPort);
  Udp.write(sBuffer);
  Udp.endPacket();
}
#endif


void freqInterupt() {

  static unsigned long prevMicros;
  unsigned long curMicros = micros() ;
  static float prevOmega;
  float omega;
  float alpha;
  unsigned int locDiff;
  static byte firstInt = 1;
  byte locPos = sCirBuf.curPos;


  if (++locPos >= CIRCBUF) {
    locPos = 0;
  }
  // sCirBuf.t_micros[locPos] = curMicros;
  // prevMicros = sCirBuf.t_micros[sCirBuf.curPos]; //TODO
  if (curMicros >= prevMicros) {
    locDiff = curMicros - prevMicros;
  } else {
    locDiff = (0xffffffff - prevMicros) + curMicros;
  }
  sCirBuf.t_diff[locPos] = locDiff;
  if (locDiff) {
    omega = 200000.0 / locDiff; //1000µs -> 200 percentage
  } else {
    omega = 0.0 ; //instead of positive inifity
  }

  alpha = (omega - prevOmega) / (float)locDiff * 1E6;

  sCirBuf.omega[locPos] = omega;
  sCirBuf.alpha[locPos] = alpha;
  prevOmega = omega;
  prevMicros = curMicros;
  sCirBuf.curPos = locPos;


}

void omegaEval(float *speedFdb, float *accFdb) {
  *speedFdb = sCirBuf.omega[sCirBuf.curPos];
  *accFdb = sCirBuf.alpha[sCirBuf.curPos];
}

#ifdef PDA
void PDA_write() {
  //wiring digital inputs to pda w1
  word tword = 0;
  tword |= PB_startAuto ? 0x0001 : 0;
  tword |= PB_pauseAuto ? 0x0002 : 0;
  tword |= SW_30cm ? 0x0004 : 0;
  tword |= SW_50cm ? 0x0008 : 0;
  tword |= PB_crossConveyor ? 0x0010 : 0;
  tword |= PB_logLifterUp ? 0x0020 : 0;
  tword |= PB_logLifterDown ? 0x0040 : 0;
  tword |= PB_saw ? 0x0080 : 0;
  tword |= PB_closeClamp ? 0x0100 : 0;
  tword |= PB_driveForward ? 0x0200 : 0;
  tword |= PB_driveBackward ? 0x0400 : 0;
  tword |= PB_reset ? 0x0800 : 0;
  tword |= LB_sawArea ? 0x1000 : 0;
  tword |= LB_splitArea ? 0x2000 : 0;
  tword |= PS_sawEndPos ? 0x4000 : 0;
  tword |= PB_startAuto_RT  ? 0x8000 : 0;
  PDA_Tel.w1 = tword;

  //wiring digital outputs to pda w2
  tword = 0;
  tword |= O_driveForward ? 0x0001 : 0;
  tword |= O_driveBackward ? 0x0002 : 0;
  tword |= O_logLifterUp ? 0x0004 : 0;
  tword |= O_logLifterDown ? 0x0008 : 0;
  tword |= O_crossConveyorForward ? 0x0010 : 0;
  tword |= O_crossConveyorBackward ? 0x0020 : 0;
  tword |= O_closeClampSaw ? 0x0040 : 0;
  tword |= O_openClampLiftSaw ? 0x0080 : 0;
  tword |= O_saw ? 0x0100 : 0;
  //tword |= false ? 0x0200 : 0;
  //tword |= false ? 0x0400 : 0;
  tword |= gen_ilk ? 0x0800 : 0;
  tword |= runSaw_ilk ? 0x1000 : 0;
  tword |= driveForward_ilk ? 0x2000 : 0;
  tword |= driveBackward_ilk ? 0x4000 : 0;
  tword |= crossConveyorForward_ilk ? 0x8000 : 0;
  PDA_Tel.w2 = tword;


  bool AUTO_liftSaw;
  bool AUTO_saw;
  bool AUTO_liftConveyor;
  bool AUTO_revertConveyor;
  bool stateChangeTriggered;
  //wiring digital outputs to pda w2
  tword = 0;
  tword |= AUTO_driveForward ? 0x0001 : 0;
  tword |= AUTO_driveBackward ? 0x0002 : 0;
  tword |= AUTO_closeClamp ? 0x0004 : 0;
  tword |= AUTO_openClamp ? 0x0008 : 0;
  tword |= AUTO_liftSaw ? 0x0010 : 0;
  tword |= AUTO_saw ? 0x0020 : 0;
  tword |= AUTO_liftConveyor ? 0x0040 : 0;
  tword |= AUTO_revertConveyor ? 0x0080 : 0;
  tword |= stateChangeTriggered ? 0x0100 : 0;/*
  //tword |= false ? 0x0200 : 0;
  //tword |= false ? 0x0400 : 0;
  tword |= gen_ilk ? 0x0800 : 0;
  tword |= runSaw_ilk ? 0x1000 : 0;
  tword |= driveForward_ilk ? 0x2000 : 0;
  tword |= driveBackward_ilk ? 0x4000 : 0;
  tword |= crossConveyorForward_ilk ? 0x8000 : 0;*/
  PDA_Tel.w3 = tword;

  //wiring integer values
  PDA_Tel.i1 = mode;
  PDA_Tel.i2 = clampState;
  PDA_Tel.i3 = sawState;
  PDA_Tel.i4 = conveyorState;
  PDA_Tel.i5 = state;
  PDA_Tel.i6 = action;
  PDA_Tel.i7 = 0;
  PDA_Tel.i8 = AI_distance;
  PDA_Tel.i9 = AI_torque;
  PDA_Tel.i10 = AI_pressure;
  PDA_Tel.i11 = sSystem.cyclTime;
  PDA_Tel.i12 = debugCount;
  PDA_Tel.i13 = O_speed;
  PDA_Tel.i14 = 0;

  //wiring float values
  PDA_Tel.f1 = speedSetpoint; //setpoint from sequence [1/100]
  PDA_Tel.f2 = speedRamp; //ramp speed output 0..200 [1/100]
  PDA_Tel.f3 = DI_freq; //evaluated frequency from interupt
  //  PDA_Tel.f4 = PT1(1, sCirBuf.omega[sCirBuf.curPos]);
  //  PDA_Tel.f5 = PT1(2, speedFdb);

  PDA_Tel.f4 = sCirBuf.omega[sCirBuf.curPos];
  PDA_Tel.f5 = speedFdb;
  PDA_Tel.f6 = PT1(3, accFdb);
  PDA_Tel.f7 = accFdb;
  PDA_Tel.f8 = DIFF(0,rel_O_speed);
  //  PDA_Tel.f9 = 0.0;
  //  PDA_Tel.f10 = 0.0;
  //  PDA_Tel.f11 = 0.0;
  //  PDA_Tel.f12 = 0.0;
  PDA_Tel.f13 = sRamp.max;
  PDA_Tel.f14 = systemPressure;
  PDA_Tel.f15 = tSpeedSetpoint; //test speed setpoint from simulation pannel

  PDA_Tel.ul1 = micros();
  PDA_Tel.ul2 = 0.0;
  PDA_Tel.ul3 = sCirBuf.t_diff[sCirBuf.curPos];
  PDA_Tel.ul4 = sCirBuf.curPos;
  PDA_Tel.ul5 = 0;
  PDA_Tel.ul6 = 0;
  PDA_Tel.ul7 = 0;

  Udp.beginPacket(PDA_Ip, PDA_Port);
  Udp.write((char*)&PDA_Tel, (int)sizeof(PDA_Tel));
  Udp.endPacket();
}
#endif
