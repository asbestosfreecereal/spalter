#ifndef IO_HANDLING_H
#define IO_HANDLING_H

#ifdef SIM 
// struct for status telegram
struct {
  char b[4];
  float f1;
  float f2;
} statusTel;
#endif

#ifdef PDA 
// struct for ProcessDataAcquisition telegram
struct {
  float f1;
  float f2;
  float f3;
  float f4;
  float f5;
  float f6;
  float f7;
  float f8;
  float f9;
  float f10;
  float f11;
  float f12;
  float f13;
  float f14;
  float f15;
  int i1;
  int i2;
  int i3;
  int i4;
  int i5;
  int i6;
  int i7;
  int i8;
  int i9;
  int i10;
  int i11;
  int i12;
  int i13;
  int i14;
  word w1; // virtual data PB and ilk
  word w2; // virtual data O
  word w3; // virtual data auto bits
  word w4;
  unsigned long ul1;
  unsigned long ul2;
  unsigned long ul3;
  unsigned long ul4;
  unsigned long ul5;
  unsigned long ul6;
  unsigned long ul7;
} PDA_Tel;

#endif

// functionality relays outputs
bool O_driveForward;
bool O_driveBackward;
bool O_closeClampSaw;
//only works if clamp is closed
bool O_saw;
bool O_openClampLiftSaw;
bool O_logLifterUp;
bool O_logLifterDown;
bool O_crossConveyorForward;
bool O_crossConveyorBackward;
//bool O_trigInterupt; //for testing purposes only
int O_speed;
float rel_O_speed; //intermediate speed just for testing purposes

//inputs
double splitLength;
int logStartDistance;

bool PB_startAuto;
bool PB_startAuto_RT;
bool PB_pauseAuto;
bool PB_driveForward;
bool PB_driveBackward;
bool PB_closeClamp;
bool PB_saw;
bool PB_logLifterUp;
bool PB_logLifterDown;
bool PB_crossConveyor;
bool LB_sawArea;
bool LB_splitArea;
bool SW_50cm;
bool SW_30cm;
bool PS_sawEndPos;

//virtual button see  PB_pauseAuto
bool PB_reset;

float speedSetpoint; //motor setpoint (100.0 = 750rpm)
float speedRamp;

float AI_distance
#ifdef SIM
= 200.0 //so simulation always starts with realistic distance val
#endif
;

float driveDistance;

float AI_pressure;
float systemPressure;
float AI_torque;

float DI_freq; // frequency evaluated on interrupt input

void setupIO();
void readInput();
void writeOutput();
#endif
