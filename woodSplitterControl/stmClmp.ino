#include "Timebase.h"
#include "hFun.h"
/*
   calculates the current clampState only switches from active to passive state if correct action is set
   to avoid sawAction triggering state change
*/
void calcClampStates() {
  bool calcState;
  for ( int i = 0; i < 2; i++) {
    calcState = (i == 0);
    switch (clampState) {
      case cls_0_CLMPUNDEF:
        if (calcState) {
          if (PB_closeClamp && modeManual && !sawSelected) {
            clampState = cls_4_CLOSING;
#ifdef LOG
            writeLog("clampState = CLOSING");
#endif
          } else if ((AUTO_openClamp && modeAuto) && systemPressure < MAX_PRESSURE_SAW) {
            clampState = cls_1_OPENING;
#ifdef LOG
            writeLog("clampState = OPENING");
#endif
          }
        } else {
          openClamp_stm = LOW;
          closeClamp_stm = LOW;
          clampOpen = LOW;
          clampClosed = LOW;
        }
        break;
      case cls_1_OPENING:
        if (calcState) {
          if (modePauseAuto ) {
            clampState = cls_2_PAUSE_OPENING;
#ifdef LOG
            writeLog("clampState = PAUSE_OPENING");
#endif
          } else if (systemPressure > MAX_PRESSURE_CLAMP) {
            clampState = cls_3_OPEN;
            speedSetpoint = SPD_IDLE;
#ifdef LOG
            writeLog("clampState = OPEN");
#endif
          } else if (mode == mod_0_MANUAL && PB_closeClamp) {
            clampState = cls_4_CLOSING;
#ifdef LOG
            writeLog("clampState = CLOSING");
#endif
          }
        } else {
          openClamp_stm = HIGH;
          closeClamp_stm = LOW;
          clampOpen = LOW;
          clampClosed = LOW;
          speedSetpoint = SPD_CLMP;
        }
        break;
      case cls_2_PAUSE_OPENING:
        if (calcState) {
          if (!modePauseAuto ) {
            clampState = cls_1_OPENING;
#ifdef LOG
            writeLog("clampState = OPENING");
#endif
          }
        } else {
          openClamp_stm = LOW;
          closeClamp_stm = LOW;
          clampOpen = LOW;
          clampClosed = LOW;
        }
        break;
      case cls_3_OPEN:
        if (calcState) {
          if ((modeManual && PB_closeClamp)
              || (modeAuto && AUTO_closeClamp && systemPressure < MAX_PRESSURE_CLAMP)) {
            clampState = cls_4_CLOSING;
#ifdef LOG
            writeLog("clampState = CLOSING");
#endif
          }
        } else {
          openClamp_stm = LOW;
          closeClamp_stm = LOW;
          clampOpen = HIGH;
          clampClosed = LOW;
        }
        break;
      case cls_4_CLOSING:
        if (calcState) {
          if (modePauseAuto ) {
            clampState = cls_5_PAUSE_CLOSING;
          } else if (systemPressure > MAX_PRESSURE_CLAMP) {
            clampState = cls_6_CLOSED;
            speedSetpoint = SPD_IDLE;
#ifdef LOG
            writeLog("clampState = CLOSED");
#endif
          } else if ((modeManual && !PB_closeClamp)) {
            clampState = cls_1_OPENING;
#ifdef LOG
            writeLog("clampState = OPENING");
#endif
          }
        } else {
          openClamp_stm = LOW;
          closeClamp_stm = HIGH;
          clampOpen = LOW;
          clampClosed = LOW;
          speedSetpoint = SPD_CLMP;
        }
        break;
      case cls_5_PAUSE_CLOSING:
        if (calcState) {
          if (!modePauseAuto ) {
            clampState = cls_4_CLOSING;
#ifdef LOG
            writeLog("clampState = CLOSING");
#endif
          }
        } else {
          openClamp_stm = LOW;
          closeClamp_stm = LOW;
          clampOpen = LOW;
          clampClosed = LOW;
        }
        break;
      case cls_6_CLOSED:
        if (calcState) {
          if ((modeManual && !PB_closeClamp && !sawSelected)
              || (modeAuto  && AUTO_openClamp && systemPressure < MAX_PRESSURE_CLAMP)) {
            clampState = cls_1_OPENING;
#ifdef LOG
            writeLog("clampState = OPENING");
#endif
          }
        } else {
          openClamp_stm = LOW;
          closeClamp_stm = LOW;
          clampOpen = LOW;
          clampClosed = HIGH;
        }
        break;
    }
  }
}
