#include "IO_handling.h"

#include "settings.h"

/*set pin modes of all IO pins*/
void setupIO() {
  //set the PIN.modes
  //PB: PushButton
  //LB: LightBarrier
  //PS: ProximitySwitch
  //AI: AnalogInput
  //DI: DigialInput
  //SW: SelectorSwitch
  //O : Output

  //digital inputs
  pinMode(CONTROLLINO_DI0, INPUT); //PB_startAuto
  pinMode(CONTROLLINO_DI1, INPUT); //PB_pauseAuto
  pinMode(CONTROLLINO_DI2, INPUT); //PB_driveForward
  pinMode(CONTROLLINO_DI3, INPUT); //PB_driveBackward
  pinMode(CONTROLLINO_AI11, INPUT); //PB_closeClamp;
  pinMode(CONTROLLINO_AI10, INPUT); //PB_saw;
  pinMode(CONTROLLINO_AI9, INPUT); //PB_logLifterUp;
  pinMode(CONTROLLINO_AI8, INPUT); //PB_logLifterDown;
  pinMode(CONTROLLINO_AI7, INPUT); //PB_crossConveyor;
  pinMode(CONTROLLINO_AI6, INPUT); //LB_sawArea;
  pinMode(CONTROLLINO_AI5, INPUT); //LB_splitArea;
  pinMode(CONTROLLINO_AI4, INPUT); //SW_50cm;
  pinMode(CONTROLLINO_AI3, INPUT); //SW_30cm;
  pinMode(CONTROLLINO_AI2, INPUT); //PS_sawEndPos;
  //interrupt inputs
  pinMode(CONTROLLINO_IN0, INPUT); //DI_frequencyInterrupt;
  //analog inputs
  pinMode(CONTROLLINO_AI13, INPUT); //AI_distance;
  pinMode(CONTROLLINO_AI0, INPUT); //AI_pressure; //to compare to torque
  pinMode(CONTROLLINO_AI12, INPUT); //AI_torque;
  //analog outputs
  pinMode(CONTROLLINO_AO0, OUTPUT); //O_speed range 0..255
  //relay outputs
  pinMode(CONTROLLINO_R0, OUTPUT);//O_driveForward
  pinMode(CONTROLLINO_R1, OUTPUT);//O_driveBackward
  pinMode(CONTROLLINO_R2, OUTPUT);//O_closeClampSaw
  pinMode(CONTROLLINO_R3, OUTPUT);//O_saw
  pinMode(CONTROLLINO_R4, OUTPUT);//O_openClampLiftSaw
  pinMode(CONTROLLINO_R5, OUTPUT);//O_logLifterUp
  pinMode(CONTROLLINO_R6, OUTPUT);//O_logLifterDown
  pinMode(CONTROLLINO_R7, OUTPUT);//O_crossConveyorForward
  pinMode(CONTROLLINO_R8, OUTPUT);//O_crossConveyorBackward
  //pinMode(CONTROLLINO_DO0, OUTPUT);//O_trigInterupt for testing purposes only
  attachInterrupt(digitalPinToInterrupt(CONTROLLINO_IN1), freqInterupt,
                  FALLING); //TODO use RISING if rising slope is steeper
}

void readInput() {
#ifdef SIM
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
#if DEBUG >= 3
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
#endif
    // read the packet into packetBufffer
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    int packetSize = Udp.parsePacket();
    packetSize = packetSize; //TODO
    Serial.println("Contents:");
    Serial.println(packetBuffer);
    byte SimByte1 = packetBuffer[0] - 64;
    byte SimState = packetBuffer[1] - 64;

    switch ( SimByte1 ) {
      case 1:
        PB_startAuto = SimState;
        break;
      case 2:
        PB_pauseAuto = SimState;
        break;
      case 3:
        SW_30cm = SimState;
        break;
      case 4:
        SW_50cm = SimState;
        break;
      case 5:
        PB_crossConveyor = SimState;
        break;
      case 6:
        PB_logLifterUp = SimState;
        break;
      case 7:
        PB_logLifterDown = SimState;
        break;
      case 8:
        PB_saw = SimState;
        break;
      case 9:
        PB_closeClamp = SimState;
        break;
      case 10:
        PB_driveForward = SimState;
        break;
      case 11:
        PB_driveBackward = SimState;
        break;
      case 12:
        PS_sawEndPos = SimState;
        break;
      case 13:
        LB_sawArea = SimState;
        break;
      case 14:
        LB_splitArea = SimState;
        break;
      case 15:
        //reset
        sawState = saw_0_SAWUNDEF;
        clampState = cls_0_CLMPUNDEF;
        sawSelected = LOW;
        PB_startAuto = LOW;
        PB_pauseAuto = LOW;
        PB_crossConveyor = LOW;
        PB_logLifterUp = LOW;
        PB_logLifterDown = LOW;
        PB_saw = LOW;
        PB_closeClamp = LOW;
        PB_driveForward = LOW;
        PB_driveBackward = LOW;
        //       O_trigInterupt = SimState;
        break;
      case 26: //"Z"
        if (SimState == 1) { //"A"
          AI_distance = (((packetBuffer[2] - 48) * 10 + (packetBuffer[3] - 48)) * 10 + (packetBuffer[4] - 48)) * 10 + (packetBuffer[5] - 48);
        } else if (SimState == 2) { //"B"
          AI_pressure = (((packetBuffer[2] - 48) * 10 + (packetBuffer[3] - 48)) * 10 + (packetBuffer[4] - 48)) * 10 + (packetBuffer[5] - 48);
        } else if (SimState == 5) { //"E"
          tSpeedSetpoint = (((packetBuffer[2] - 48) * 10 + (packetBuffer[3] - 48)) * 10 + (packetBuffer[4] - 48)) * 10 + (packetBuffer[5] - 48);
        }
        break;
      default:
        Serial.println("ERROR");
        break;
    }
  }

  if (O_driveForward) {
    AI_distance += speedRamp * sSystem.cyclTime * 0.0005 ;
    if ( AI_distance > ENDPOS_FWD) AI_distance = ENDPOS_FWD;
  }

  if (O_driveBackward) {
    AI_distance -= speedRamp * sSystem.cyclTime * 0.0005 ;
    if ( AI_distance < ENDPOS_BWD) AI_distance = ENDPOS_BWD;
  }

  systemPressure = AI_pressure ;
  driveDistance = AI_distance ;

#else
  /*read all external inputs and hold them for one cycle*/
  PB_startAuto     = digitalRead(CONTROLLINO_DI0);
  PB_pauseAuto     = digitalRead(CONTROLLINO_DI1);
  PB_driveForward  = digitalRead(CONTROLLINO_DI2);
  PB_driveBackward = digitalRead(CONTROLLINO_DI3);
  PB_closeClamp    = digitalRead(CONTROLLINO_AI11);
  PB_saw           = digitalRead(CONTROLLINO_AI10);
  PB_logLifterUp   = digitalRead(CONTROLLINO_AI9);
  PB_logLifterDown = digitalRead(CONTROLLINO_AI8);
  PB_crossConveyor = digitalRead(CONTROLLINO_AI7);
  LB_sawArea       = digitalRead(CONTROLLINO_AI6);
  LB_splitArea     = digitalRead(CONTROLLINO_AI5);
  SW_50cm          = digitalRead(CONTROLLINO_AI4);
  SW_30cm          = digitalRead(CONTROLLINO_AI3);
  PS_sawEndPos     = digitalRead(CONTROLLINO_AI2);

  //TODO map and scale properly; double check before working on live system
  AI_torque = map(analogRead(CONTROLLINO_AI12), 0, 1023, 0, 200); //torque analog in map to %
  AI_distance = map(analogRead(CONTROLLINO_AI13), 0, 1013, 200, 400); //TODO adapt mapping to machine conditions
  AI_pressure = map(analogRead(CONTROLLINO_AI12), 0, 511, 0, 200); //pressure analog in map to % ()

  systemPressure = AI_pressure / 1024.0 * 250.0;
  driveDistance = AI_distance ;
#endif

  if (SW_50cm) {
    splitLength = FIFTY_CM;
  } else if (SW_30cm) {
    splitLength = THIRTYTHREE_CM;
  } else {
    splitLength = TWENTYFIVE_CM;
  }
  PB_reset = F_TRIG(1, TON(1, PB_pauseAuto));
  PB_startAuto_RT = R_TRIG(1, PB_startAuto);
}

void writeOutput() {
  O_speed = map(rel_O_speed, 0.0, 200.0, 0, 255);
  //digitalWrite(CONTROLLINO_DO0, O_trigInterupt); //for testing purposes only
#ifdef SIM
  statusTel.b[0] = 64;
  statusTel.b[1] = (O_driveForward ? 1 : 0 ) + (O_driveBackward ? 2 : 0 ) +
                   (O_logLifterUp ? 4 : 0 ) + (O_logLifterDown ? 8 : 0 ) +
                   (O_crossConveyorForward ? 16 : 0 ) ; //3 spare bits
  statusTel.b[2] = (O_crossConveyorBackward ? 1 : 0 ) + (O_closeClampSaw ? 2 : 0 ) +
                   (O_openClampLiftSaw ? 4 : 0 ) + (O_saw ? 8 : 0 ) ; //4 spare bits
  statusTel.b[3] = sSystem.cyclTime;
  statusTel.f1 = driveDistance;
  //statusTel.f2 = speedRamp * 7.5;
  statusTel.f2 = DI_freq;
  if (TON(29, 1)) {
    TON(29, 0);
    // send a reply
    Udp.beginPacket(remIp, remPort);
    Udp.write((char*)&statusTel, (int)sizeof(statusTel));
    Udp.endPacket();
  }
#else
  digitalWrite(CONTROLLINO_R0, O_driveForward);
  digitalWrite(CONTROLLINO_R1, O_driveBackward);
  digitalWrite(CONTROLLINO_R2, O_closeClampSaw);
  digitalWrite(CONTROLLINO_R3, O_saw);
  digitalWrite(CONTROLLINO_R4, O_openClampLiftSaw);
  digitalWrite(CONTROLLINO_R5, O_logLifterUp);
  digitalWrite(CONTROLLINO_R6, O_logLifterDown);
  digitalWrite(CONTROLLINO_R7, O_crossConveyorForward);
  digitalWrite(CONTROLLINO_R8, O_crossConveyorBackward);
  analogWrite(CONTROLLINO_AO0, O_speed);
#endif
}
