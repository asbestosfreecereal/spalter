#include "Timebase.h"
#include "hFun.h"

#include "stmAuto.h"


void sequenceAuto() {
  int calcState;
  for (int i = 0; i < 2; i++) {
    calcState = (i == 0);
    switch (state) {

      case sts_0_AUTO_OFF:
        break;
      case sts_5_AUTO_ERROR:
        if (calcState) {
          if (PB_driveForward || PB_driveBackward || PB_closeClamp || PB_saw || PB_logLifterUp || PB_logLifterDown || PB_crossConveyor) {
            state = sts_0_AUTO_OFF;
            mode = mod_0_MANUAL;
#ifdef LOG
            writeLog("stat=AUTO_OFF");
            writeLog("mode=MANUAL");
#endif
          }
        } else {
          AUTO_closeClamp = LOW;
          AUTO_openClamp = LOW;
          AUTO_liftSaw = LOW;
          AUTO_saw = LOW;
          AUTO_driveForward = LOW;
          AUTO_driveBackward = LOW;
          AUTO_liftConveyor = LOW;
          AUTO_revertConveyor = LOW;
          stateChangeTriggered = LOW;
          splitCount = 0;
          sawDone = LOW;

        }
        break;
      case sts_1_LOAD_LOG:
        switch (action) {
          case act_1_CONVEYOR_FORWARD:
            if (calcState) {
              if (conveyorState == con_3_CONVEYOR_FRONT) {
                action = act_2_CONVEYOR_BACKWARD;
#ifdef LOG
                writeLog("action = CONVEYOR_BACKWARD");
#endif
              }
            } else {

              AUTO_driveForward = LOW;
              AUTO_driveBackward = LOW;
              AUTO_liftConveyor = HIGH;
            }
            break;
          case act_2_CONVEYOR_BACKWARD:
            if (calcState) {
              if (conveyorState == con_6_CONVEYOR_BACK) {
                action = act_3_DRIVE_FORWARD;
#ifdef LOG
                writeLog("action = DRIVE_FORWARD;");
#endif
              }
            } else {
              AUTO_liftConveyor = LOW;
              AUTO_revertConveyor = HIGH;
            }
            break;
          case act_3_DRIVE_FORWARD:
            if (calcState) {
              if (LB_sawArea) {
                if (ENDPOS_FWD - driveDistance < MIN_FWD_LENGTH_CLMP) {
                  //TODO abort because clamp can't possibly grip log
                  mode = mod_2_PAUSE;
#ifdef LOG
                  writeLog("mode = PAUSE;");
#endif
                  action = act_0_NONE;
#ifdef LOG
                  writeLog("action = NONE;");
#endif
                } else {
                  state = sts_2_POSITION_LOG;
#ifdef LOG
                  writeLog("state = POSITION_LOG;");
#endif
                  logStartPos = driveDistance;
#ifdef LOG
                  writeLog("logStartPos = driveDistance;");
#endif
                }
              } else if (driveDistance >= ENDPOS_FWD) {
                action = act_4_DRIVE_BACKWARD;
#ifdef LOG
                writeLog("action = DRIVE_BACKWARD;");
#endif
              }
            } else {
              AUTO_revertConveyor = LOW;
              AUTO_driveForward = HIGH;
              speedSetpoint = SPD_DRV_FWD_FAST;
            }
            break;
          case act_4_DRIVE_BACKWARD:
            if (calcState) {
              if (driveDistance <= ENDPOS_BWD) {
                action = act_1_CONVEYOR_FORWARD;
#ifdef LOG
                writeLog("action = CONVEYOR_FORWARD;");
#endif
              } else {
                AUTO_driveForward = LOW;
                AUTO_driveBackward = HIGH;
                speedSetpoint = SPD_DRV_BWD_FAST;
              }
              break;
            default:
              break;
            }
        }
        break;
      case sts_2_POSITION_LOG:
        switch (action) {
          case act_3_DRIVE_FORWARD:
            if (calcState) {
              if (driveDistance >= (logStartPos + FWD_LENGTH_CLMP) || driveDistance >= ENDPOS_FWD) {
                action = act_5_CLOSE_CLAMP;
#ifdef LOG
                writeLog("action = CLOSE_CLAMP;");
#endif
              }
            } else {
              AUTO_driveForward = HIGH;
              speedSetpoint = SPD_DRV_FWD;
            }
            break;
          case act_5_CLOSE_CLAMP:
            if (calcState) {
              if (clampState == cls_6_CLOSED) {
                action = act_4_DRIVE_BACKWARD;
#ifdef LOG
                writeLog("action = DRIVE_BACKWARD;");
#endif
              }
            } else {
              AUTO_driveForward = LOW;
              AUTO_closeClamp = HIGH;
            }
            break;
          case act_4_DRIVE_BACKWARD:
            if (calcState) {
              if (driveDistance <= ENDPOS_BWD) {
                action = act_6_OPEN_CLAMP;
#ifdef LOG
                writeLog("action = OPEN_CLAMP (position log);");
#endif
              }
            } else {
              AUTO_closeClamp = LOW;
              AUTO_driveBackward = HIGH;
            }
            break;
          case act_6_OPEN_CLAMP:
            if (calcState) {
              if (clampState == cls_3_OPEN) {
                state = sts_3_SAW_LOG;
#ifdef LOG
                writeLog("state = SAW_LOG");
#endif
                action = act_3_DRIVE_FORWARD;
#ifdef LOG
                writeLog("action = DRIVE_FORWARD");
#endif
              }
            } else {
              AUTO_driveBackward = LOW;
              AUTO_openClamp = HIGH;
            }
            break;
          default:
            break;
        }
        break;
      case sts_3_SAW_LOG:
        switch (action) {
          case act_3_DRIVE_FORWARD:
            if (calcState) {
              if (driveDistance >= (ENDPOS_BWD + splitLength - FWD_LENGTH_CLMP)) {
                action = act_5_CLOSE_CLAMP;
#ifdef LOG
                writeLog("action = CLOSE_CLAMP;");
#endif
              }
            } else {
              AUTO_openClamp = LOW;
              AUTO_driveForward = HIGH;
            }
            break;
          case act_5_CLOSE_CLAMP:
            if (calcState) {
              if (clampState == cls_6_CLOSED) {
                action = act_4_DRIVE_BACKWARD;
#ifdef LOG
                writeLog("action = DRIVE_BACKWARD");
#endif
              }
            } else {
              AUTO_closeClamp = HIGH;
              AUTO_driveForward = LOW;
            }
            break;
          case act_4_DRIVE_BACKWARD:
            if (calcState) {
              if (sawDone) {
                if (driveDistance <= ENDPOS_BWD) {
                  action = act_6_OPEN_CLAMP;
#ifdef LOG
                  writeLog("action = OPEN_CLAMP (saw log)");
#endif
                }
              } else {
                if (driveDistance <= SAW_POSITION) {
                  action = act_7_SAW;
#ifdef LOG
                  writeLog("action = SAW");
#endif
                }
              }
            } else {
              AUTO_closeClamp = LOW;
              AUTO_driveBackward = HIGH;
              AUTO_liftSaw = LOW;
            }
            break;
          case act_7_SAW:
            if (calcState) {
              if (TON(2, 1)) { //check max saw time
                TON(2, 0); //reset max saw time
                state = sts_5_AUTO_ERROR;
                sawState = saw_0_SAWUNDEF;
#ifdef LOG
                writeLog("state = AUTO_ERROR (max saw time)");
#endif

              }
              if (sawState == saw_4_DOWN) {
                TON(2, 0); //reset max saw time
                action = act_8_LIFT_SAW;
#ifdef LOG
                writeLog("action = LIFT_SAW");
#endif
              }
            } else {
              sawDone = true;
              AUTO_driveBackward = LOW;
              AUTO_liftSaw = LOW;
              AUTO_saw = HIGH;
            }
            break;
          case act_8_LIFT_SAW:
            if (calcState) {
              if (sawState == saw_2_UP) {
                action = act_4_DRIVE_BACKWARD;
#ifdef LOG
                writeLog("action = DRIVE_BACKWARD");
#endif
              }
            } else {
              AUTO_driveBackward = LOW;
              AUTO_saw = LOW;
              AUTO_liftSaw = HIGH;
            }
            break;
          case act_6_OPEN_CLAMP:
            if (calcState) {
              if (clampState == cls_3_OPEN) {
                state = sts_4_SPLIT_WOOD;
                action = act_9_WAIT_FOR_WOOD;
#ifdef LOG
                writeLog("state = sts_4_SPLIT_WOOD");
                writeLog("action = act_9_WAIT_FOR_WOOD");
#endif

              }
            } else {
              sawDone = false;
              AUTO_driveBackward = LOW;
              AUTO_openClamp = HIGH;
              splitCount = 0;
            }
            break;
          default:
            break;
        }
        break;
      case sts_4_SPLIT_WOOD:
        switch (action) {

          case act_9_WAIT_FOR_WOOD:
            if (LB_splitArea) {
              action = act_3_DRIVE_FORWARD;
            } else if (!LB_splitArea && TON(4, 1)) {
              TON(4, 0);
              state = sts_5_AUTO_ERROR;
#ifdef LOG
              writeLog("state = sts_5_AUTO_ERROR");
#endif
            }
            break;
          case act_3_DRIVE_FORWARD:
            if (calcState) {
              if (splitCount == 0 && !LB_splitArea) {
                state = sts_5_AUTO_ERROR;
#ifdef LOG
                writeLog("state = AUTO_ERROR (no wood)");
#endif
              } else if (splitCount > 5) {
                state = sts_5_AUTO_ERROR;
#ifdef LOG
                writeLog("state = AUTO_ERROR (max count)");
#endif
              } else if (!LB_splitArea) {
                //drive forward to check if LB_sawArea is triggered
                stateChangeTriggered = true;
              }
              if (stateChangeTriggered && driveDistance <= ENDPOS_FWD) {
                if (LB_sawArea) {
                  stateChangeTriggered = false;
                  logStartPos = driveDistance;
#ifdef LOG
                  writeLog("logStartPos = driveDistance");
#endif
                  state = sts_2_POSITION_LOG;
#ifdef LOG
                  writeLog("state = POSITION_LOG");
#endif
                }
              }
              if (stateChangeTriggered && driveDistance >= ENDPOS_FWD) {
                stateChangeTriggered = false;
                state = sts_1_LOAD_LOG;
#ifdef LOG
                writeLog("state = LOAD_LOG");
#endif
                action = act_4_DRIVE_BACKWARD;
#ifdef LOG
                writeLog("action = DRIVE_BACKWARD");
#endif
              }
              if (!stateChangeTriggered && driveDistance >= ENDPOS_FWD) {
                action = act_4_DRIVE_BACKWARD;
                splitCount++;
#ifdef LOG
                writeLog("action = DRIVE_BACKWARD");
#endif
              }
            } else {
              AUTO_openClamp = LOW;
              AUTO_driveForward = HIGH;
              AUTO_driveBackward = LOW;
            }
            break;
          case act_4_DRIVE_BACKWARD:
            if (calcState) {
              if (driveDistance <= (ENDPOS_FWD - (splitLength + FIVE_CM))) {
                action = act_3_DRIVE_FORWARD;
#ifdef LOG
                writeLog("action = DRIVE_FORWARD");
#endif
              }
            } else {
              AUTO_driveForward = LOW;
              AUTO_driveBackward = HIGH;
            }
            break;
          default:
            ;
            break;
        }
        break;
      case sts_6_INIT_AUTO:
        switch (action) {
          case act_8_LIFT_SAW:
            if (calcState) {
              if (sawState == saw_2_UP) {
                action = act_6_OPEN_CLAMP;
#ifdef LOG
                writeLog("action = act_6_OPEN_CLAMP");
#endif
              }
            } else {
              AUTO_liftSaw = HIGH;
            }
            break;
          case act_6_OPEN_CLAMP:
            if (calcState) {
              if (clampState == cls_3_OPEN) {
                action = act_4_DRIVE_BACKWARD;
#ifdef LOG
                writeLog("action = act_4_DRIVE_BACKWARD");
#endif
              }
            } else {
              AUTO_liftSaw = LOW;
              AUTO_openClamp = HIGH;
            }
            break;
          case  act_4_DRIVE_BACKWARD:
            if (calcState) {
              if (driveDistance <= ENDPOS_BWD) {
                action = act_2_CONVEYOR_BACKWARD;
#ifdef LOG
                writeLog("action = act_2_CONVEYOR_BACKWARD");
#endif
              }
            } else {
              AUTO_openClamp = LOW;
              AUTO_driveBackward = HIGH;
              speedSetpoint = SPD_DRV_BWD_FAST;
            }
            break;
          case act_2_CONVEYOR_BACKWARD:
            if (calcState) {
              if (conveyorState == con_6_CONVEYOR_BACK && !LB_sawArea) {
                //inititialization of auto mode done
                if (!PB_startAuto) {
                  state = sts_1_LOAD_LOG;
                  action = act_1_CONVEYOR_FORWARD;

#ifdef LOG
                  writeLog("state = sts_1_LOAD_LOG");
                  writeLog("action = act_1_CONVEYOR_FORWARD");

#endif
                } else if (TON(0, PB_startAuto) && !LB_splitArea) {
                  //long PB_sartAuto to skip cross conveyor loading
                  TON(0, 0);
                  action = act_3_DRIVE_FORWARD;
                  state = sts_1_LOAD_LOG;
#ifdef LOG
                  writeLog("action = DRIVE_FORWARD");
                  writeLog("state = LOAD_LOG");
#endif
                } else if (TON(0, PB_startAuto) && LB_splitArea) {
                  //long PB_sartAuto to skip cross conveyor loading and start splitting
                  TON(0, 0);
                  action = act_3_DRIVE_FORWARD;
                  state = sts_4_SPLIT_WOOD;
                  splitCount = 0;
#ifdef LOG
                  writeLog("action = DRIVE_FORWARD");
                  writeLog("state = SPLIT_WOOD");
#endif

                }
              } else if (conveyorState == con_6_CONVEYOR_BACK && LB_sawArea) {
                state = sts_5_AUTO_ERROR;
#ifdef LOG
                writeLog("state = sts_5_AUTO_ERROR init error: LB_sawArea");
#endif
              }
            } else {
              AUTO_driveBackward = LOW;
              AUTO_revertConveyor = HIGH;
            }
            break;
        }
        break;
    }
  }
}
