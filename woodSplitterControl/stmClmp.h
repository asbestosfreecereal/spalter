#ifndef STM_CLMP_H
#define STM_CLMP_H

typedef enum {
  cls_0_CLMPUNDEF,
  cls_1_OPENING,
  cls_2_PAUSE_OPENING,
  cls_3_OPEN,
  cls_4_CLOSING,
  cls_5_PAUSE_CLOSING,
  cls_6_CLOSED
} clampStates;

clampStates clampState = cls_0_CLMPUNDEF;

bool clampOpen;
bool clampClosed;

void calcClampStates();
#endif // STM_CLMP_H
