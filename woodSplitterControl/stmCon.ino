#include "Timebase.h"
#include "hFun.h"

void calcConveyorState() {
  bool calcState;
  for (int i = 0; i < 2; i++) {
    calcState = (i == 0);
    switch (conveyorState) {
      case con_0_CONUNDEF:
        if (calcState) {
          if ((PB_crossConveyor && (modeManual  || modePauseAuto) && (systemPressure < MAX_PRESSURE_CONVEYOR))
              || (modeAuto  && AUTO_revertConveyor && (systemPressure < MAX_PRESSURE_CONVEYOR))) {
            conveyorState = con_4_REVERT_CONVEYOR;
#ifdef LOG
            writeLog("conveyorState = REVERT_CONVEYOR;");
#endif
          } else if (modeAuto  &&  AUTO_liftConveyor  && (systemPressure < MAX_PRESSURE_CONVEYOR)) {

            conveyorState = con_1_LIFT_CONVEYOR;
#ifdef LOG
            writeLog("conveyorState = LIFT_CONVEYOR;");
#endif
          }
        } else {
          crossConveyorForward_stm = LOW;
          crossConveyorBackward_stm = LOW;
        }
        break;

      case con_1_LIFT_CONVEYOR:
        if (calcState) {
          if (systemPressure > MAX_PRESSURE_CONVEYOR) {
            conveyorState = con_2_CONVEYOR_FRONT_WAIT;
#ifdef LOG
            writeLog("conveyorState = CONVEYOR_FRONT_WAIT;");
#endif
          }
        } else {
          crossConveyorForward_stm = HIGH;
          crossConveyorBackward_stm = LOW;
          speedSetpoint = SPD_CONV;
        }
        break;

      case con_2_CONVEYOR_FRONT_WAIT:
        if (calcState) {
          if (TON(3, 1)) {
            TON(3, 0);
            conveyorState = con_3_CONVEYOR_FRONT;
#ifdef LOG
            writeLog("conveyorState = CONVEYOR_FRONT;");
#endif
          }
        } else {
          crossConveyorForward_stm = LOW;
          crossConveyorBackward_stm = LOW;
          speedSetpoint = SPD_IDLE;
        }
        break;

      case con_3_CONVEYOR_FRONT:
        if (calcState) {
          if ((PB_crossConveyor && (modeManual  || modePauseAuto) && (systemPressure < MAX_PRESSURE_CONVEYOR))
              || ((modeAuto) && AUTO_revertConveyor && (systemPressure < MAX_PRESSURE_CONVEYOR))) {
            conveyorState = con_4_REVERT_CONVEYOR;
#ifdef LOG
            writeLog("conveyorState = REVERT_CONVEYOR;");
#endif
          }
        } else {
          crossConveyorForward_stm = LOW;
          crossConveyorBackward_stm = LOW;;
        }
        break;

      case con_4_REVERT_CONVEYOR:
        if (calcState) {
          if (systemPressure > MAX_PRESSURE_CONVEYOR) {
            conveyorState = con_5_CONVEYOR_BACK_WAIT;
#ifdef LOG
            writeLog("conveyorState = CONVEYOR_BACK_WAIT;");
#endif
          }
        } else {
          crossConveyorForward_stm = LOW;
          crossConveyorBackward_stm = HIGH;
          speedSetpoint = SPD_CONV;
        }
        break;

      case con_5_CONVEYOR_BACK_WAIT:
        if (calcState) {
          if (TON(3, 1)) {
            TON(3, 0);
            conveyorState = con_6_CONVEYOR_BACK;
#ifdef LOG
            writeLog("conveyorState = CONVEYOR_BACK");
#endif
          }
        } else {
          crossConveyorForward_stm = LOW;
          crossConveyorBackward_stm = LOW;
          speedSetpoint = SPD_IDLE;
        }
        break;

      case con_6_CONVEYOR_BACK:
        if (calcState) {
          if ((PB_crossConveyor && (modeManual || modePauseAuto) && (systemPressure < MAX_PRESSURE_CONVEYOR) && !crossConveyorForward_ilk)
              || (modeAuto && AUTO_liftConveyor && (systemPressure < MAX_PRESSURE_CONVEYOR) && !crossConveyorForward_ilk)) {
            conveyorState = con_1_LIFT_CONVEYOR;
#ifdef LOG
            writeLog("conveyorState = LIFT_CONVEYOR;");
#endif
          }
        } else {
          crossConveyorForward_stm = LOW;
          crossConveyorBackward_stm = LOW;
        }
        break;

    }
  }
}
