#ifndef STM_AUTO_H
#define STM_AUTO_H

typedef enum {
  sts_0_AUTO_OFF,
  sts_1_LOAD_LOG,
  sts_2_POSITION_LOG,
  sts_3_SAW_LOG,
  sts_4_SPLIT_WOOD,
  sts_5_AUTO_ERROR,
  sts_6_INIT_AUTO
} statesAutoMode;

statesAutoMode state = sts_0_AUTO_OFF;
statesAutoMode prevState = sts_0_AUTO_OFF;

typedef enum {
  act_0_NONE,
  act_1_CONVEYOR_FORWARD,
  act_2_CONVEYOR_BACKWARD,
  act_3_DRIVE_FORWARD,
  act_4_DRIVE_BACKWARD,
  act_5_CLOSE_CLAMP,
  act_6_OPEN_CLAMP,
  act_7_SAW,
  act_8_LIFT_SAW,
  act_9_WAIT_FOR_WOOD
} actions;

actions action = act_0_NONE;
actions prevAction = act_0_NONE;



bool AUTO_closeClamp;
bool AUTO_openClamp;
bool AUTO_liftSaw;
bool AUTO_saw;
bool AUTO_driveForward;
bool AUTO_driveBackward;
bool AUTO_liftConveyor;
bool AUTO_revertConveyor;
bool stateChangeTriggered;
int splitCount;
int logStartPos;

void sequenceAuto();
#endif // STM_AUTO_H
